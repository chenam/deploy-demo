/*
 * @Author: chenamin
 * @LastModifiedBy: chenamin
 * @Date: 2021-08-18 09:50:48
 * @LastEditTime: 2021-08-19 10:56:54
 * @FilePath: /ts/src/components/ProductList.tsx
 * @Description: file content
 */
import { Table, Popconfirm, Button } from "antd";

const ProductList: React.FC<{
  products: { name: string }[];
  onDelete: (id: string) => void;
}> = ({ onDelete, products }) => {
  /** A cool guy. */
  interface Person {
    /** A cool name. */
    name: string;
  }

  const p: Person = {
    name: "aaa",
  };

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
    },
    {
      title: "Actions",
      render: (text, record) => {
        return (
          <Popconfirm title="Delete?" onConfirm={() => onDelete(record.id)}>
            <Button>Delete</Button>
          </Popconfirm>
        );
      },
    },
  ];
  return <Table dataSource={products} columns={columns} />;
};

export default ProductList;
