/*
 * @Author: chenamin
 * @LastModifiedBy: chenamin
 * @Date: 2021-08-18 09:49:12
 * @LastEditTime: 2021-08-18 10:28:06
 * @FilePath: /ts/src/pages/products.tsx
 * @Description: file content
 */
import React from "react";
import styles from "./products.css";
import ProductList from "@/components/ProductList";

export default function Page() {
  const products = [{ name: "111" }];
  const onDelete = () => {};
  return (
    <div>
      <h1 className={styles.title}>Page products</h1>
      <ProductList products={products} onDelete={onDelete} />
    </div>
  );
}
